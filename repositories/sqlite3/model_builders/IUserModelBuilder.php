<?php

require_once __DIR__ . '/../../../models/UserModel.php';

interface IUserModelBuilder
{
    /** @param array<mixed> $data */
    public function build(?array $data): ?UserModel;
}