<?php

namespace Sqlite3Repository;

require_once __DIR__ . '/BaseRepository.php';
require_once __DIR__ . '/../interfaces/IUserRepository.php';
require_once __DIR__ . '/../../db/CustomSQLite3.php';
require_once __DIR__ . '/../../models/UserModel.php';
require_once __DIR__ . '/../../exceptions/UsernameAlreadyExistsException.php';
require_once __DIR__ . '/model_builders/IUserModelBuilder.php';

use CustomSQLite3;
use IUserRepository;
use UserModel;
use UsernameAlreadyExistsException;
use Exception;
use IUserModelBuilder;

class UserRepository extends BaseRepository implements IUserRepository
{
    private IUserModelBuilder $userModelBuilder;

    public function __construct(CustomSQLite3 $dataSource, IUserModelBuilder $userModelBuilder)
    {
        parent::__construct($dataSource);
        $this->userModelBuilder = $userModelBuilder;
    }

    public function findByUsername(string $username): ?UserModel
    {
        $user = null;

        $stmt = $this->dataSource->customPrepare('SELECT
                `id`,
                `username`,
                `password`,
                `account_code`
            FROM `users`
            WHERE `username` = :username
            ORDER BY `id` DESC
            LIMIT 1
        ', __FILE__, __LINE__);

        if ($stmt !== false) {
            $stmt->bindValue(':username', $username, SQLITE3_TEXT);
        
            $result = $stmt->execute();
    
            if ($result !== false) {
                $resultData = $result->fetchArray(SQLITE3_ASSOC);
    
                if ($resultData !== false) {
                    $user = $this->userModelBuilder->build($resultData);
                }
            }
        }

        return $user;
    }

    public function existsUserWithUsername(string $username): bool
    {
        $userExists = false;

        $stmt = $this->dataSource->customPrepare('SELECT EXISTS(
            SELECT 1
            FROM `users`
            WHERE `username` = :username
        ) AS user_exists;', __FILE__, __LINE__);

        if ($stmt !== null) {
            $stmt->bindValue(':username', $username, SQLITE3_TEXT);
    
            $result = $stmt->execute();
    
            if ($result !== false) {
                $userExistsResult = $result->fetchArray();

                if ($userExistsResult !== false) {
                    $userExists = (bool) $userExistsResult['user_exists'];
                }
            }
        }

        return $userExists;
    }

    public function signUp(UserModel $user): void
    {
        if ($this->existsUserWithUsername($user->username ?? '')) {
            throw new UsernameAlreadyExistsException();
        }

        $stmt = $this->dataSource->customPrepare('INSERT INTO users
            (`username`, `password`, `account_code`)
            VALUES
            (:username, :password, :account_code);
        ', __FILE__, __LINE__);

        $result = false;

        if ($stmt !== null) {
            $stmt->bindValue(':username', $user->username, SQLITE3_TEXT);
            $stmt->bindValue(':password', $user->password, SQLITE3_TEXT);
            $stmt->bindValue(':account_code', $user->accountCode, SQLITE3_TEXT);
    
            $result = $stmt->execute();
        }

        if ($result === false) {
            throw new Exception("Error Processing Request", 1);
        }
    }
}