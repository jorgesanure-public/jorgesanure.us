<?php

namespace Sqlite3Repository;

require_once __DIR__ . '/../../db/CustomSQLite3.php';

use CustomSQLite3;

abstract class BaseRepository
{
    protected CustomSQLite3 $dataSource;

    public function __construct(CustomSQLite3 $dataSource)
    {
        $this->dataSource = $dataSource;
    }
}