<?php
namespace ArrayRepository;

require_once __DIR__ . '/interfaces/IArrayUserRepository.php';
require_once __DIR__ . '/../../models/UserModel.php';
require_once __DIR__ . '/../../exceptions/UsernameAlreadyExistsException.php';

use IArrayUserRepository;
use UserModel;
use UsernameAlreadyExistsException;

class UserRepository implements IArrayUserRepository
{
    /** @var array<string, array<mixed>> $dataSource */
    private array $dataSource;

    /** @param array<string, array<mixed>> $dataSource */
    public function __construct(array &$dataSource = null)
    {
        if (! is_array($dataSource)) {
            $dataSource = [];
        }

        if (! array_key_exists('users', $dataSource)) {
            $dataSource['users'] = [];
        }

        $this->dataSource =  $dataSource;
    }

    public function findByUsername(string $username): ?UserModel
    {
        $foundUser = null;

        if (
            ! empty($username)
            && is_array($this->dataSource)
            && array_key_exists('users', $this->dataSource)
            && is_array($this->dataSource['users'])
        ) {
            foreach ($this->dataSource['users'] as $user) {
                $userModel = null;
                if ($user instanceof UserModel) {
                    $userModel = $user;
                }
                
                if ($userModel !== null) {
                    $foundUser = $userModel->username === $username ? $userModel : null;
                    break;
                }
            }
        }

        return $foundUser;
    }

    public function existsUserWithUsername(string $username): bool
    {
        return ! empty($this->findByUsername($username));
    }

    public function signUp(UserModel $user): void
    {
        if ($this->existsUserWithUsername($user->username ?? '')) {
            throw new UsernameAlreadyExistsException();
        }

        $this->dataSource['users'][] = $user;
    }
}