<?php

require_once __DIR__ . '/../../models/UserModel.php';

interface IUserRepository
{
    public function existsUserWithUsername(string $username): bool;
    public function signUp(UserModel $user): void;
    public function findByUsername(string $username): ?UserModel;
}