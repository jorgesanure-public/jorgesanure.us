<?php

require_once __DIR__ . '/ServiceProvider.php';
require_once __DIR__ . '/../repositories/interfaces/IUserRepository.php';
require_once __DIR__ . '/../repositories/sqlite3/UserRepository.php';
require_once __DIR__ . '/../repositories/array/interfaces/IArrayUserRepository.php';
require_once __DIR__ . '/../repositories/array/UserRepository.php';
require_once __DIR__ . '/../controllers/interfaces/ISignUpController.php';
require_once __DIR__ . '/../controllers/interfaces/ILoginController.php';
require_once __DIR__ . '/../controllers/interfaces/IIndexController.php';
require_once __DIR__ . '/../controllers/interfaces/IDeployController.php';
require_once __DIR__ . '/../controllers/interfaces/IDashboardController.php';
require_once __DIR__ . '/../controllers/SignUpController.php';
require_once __DIR__ . '/../controllers/LoginController.php';
require_once __DIR__ . '/../controllers/IndexController.php';
require_once __DIR__ . '/../controllers/DeployController.php';
require_once __DIR__ . '/../controllers/DashboardController.php';
require_once __DIR__ . '/../repositories/sqlite3/model_builders/IUserModelBuilder.php';
require_once __DIR__ . '/../repositories/sqlite3/model_builders/UserModelBuilder.php';
require_once __DIR__ . '/../logs/interfaces/IAccessLogger.php';
require_once __DIR__ . '/../logs/interfaces/IDataSourceLogger.php';
require_once __DIR__ . '/../logs/interfaces/IDeployLogger.php';
require_once __DIR__ . '/../logs/interfaces/ILogger.php';
require_once __DIR__ . '/../logs/interfaces/IOldLogFileRemover.php';
require_once __DIR__ . '/../logs/AccessLogger.php';
require_once __DIR__ . '/../logs/DataSourceLogger.php';
require_once __DIR__ . '/../logs/DeployLogger.php';
require_once __DIR__ . '/../logs/Logger.php';
require_once __DIR__ . '/../logs/OldLogFileRemover.php';
require_once __DIR__ . '/../services/service_models/interfaces/IArrayUtils.php';
require_once __DIR__ . '/../services/service_models/interfaces/ISession.php';
require_once __DIR__ . '/../services/service_models/interfaces/IAuthenticator.php';
require_once __DIR__ . '/../services/service_models/interfaces/IRequest.php';
require_once __DIR__ . '/../services/service_models/interfaces/IResponse.php';
require_once __DIR__ . '/../services/service_models/interfaces/IResponseHandler.php';
require_once __DIR__ . '/../services/service_models/interfaces/IStringUtils.php';
require_once __DIR__ . '/../services/service_models/interfaces/ISitemap.php';
require_once __DIR__ . '/../services/service_models/interfaces/IRobots.php';
require_once __DIR__ . '/../services/service_models/interfaces/IEnv.php';
require_once __DIR__ . '/../services/service_models/ArrayUtils.php';
require_once __DIR__ . '/../services/service_models/Session.php';
require_once __DIR__ . '/../services/service_models/Authenticator.php';
require_once __DIR__ . '/../services/service_models/Request.php';
require_once __DIR__ . '/../services/service_models/Response.php';
require_once __DIR__ . '/../services/service_models/ResponseHandler.php';
require_once __DIR__ . '/../services/service_models/StringUtils.php';
require_once __DIR__ . '/../services/service_models/Sitemap.php';
require_once __DIR__ . '/../services/service_models/Robots.php';
require_once __DIR__ . '/../services/service_models/CustomEnv.php';
require_once __DIR__ . '/../routes/interfaces/IRouter.php';
require_once __DIR__ . '/../routes/Router.php';
require_once __DIR__ . '/../views/interfaces/IView.php';
require_once __DIR__ . '/../views/View.php';

use Sqlite3Repository\UserRepository;
use ArrayRepository\UserRepository as ArrayUserRepository;

class ServiceProviderBuilder
{
    private static ?ServiceProvider $serviceProvider;

    public function __construct()
    {
        self::$serviceProvider = null;
    }

    public function build(): ?ServiceProvider
    {
        $serviceProvider = null;

        if (! empty(self::$serviceProvider)) {
            $serviceProvider = self::$serviceProvider;
        } else {
            $serviceProvider = new ServiceProvider();

            /**
             * concrete service class map directory setting
             */
            $serviceProvider->mapClassDirectory(UserModelBuilder::class, __DIR__ . '/../repositories/sqlite3/model_builders/UserModelBuilder.php');
            $serviceProvider->mapClassDirectory(UserRepository::class, __DIR__ . '/../repositories/sqlite3/UserRepository.php');
            $serviceProvider->mapClassDirectory(ArrayUserRepository::class, __DIR__ . '/../repositories/array/UserRepository.php');
            $serviceProvider->mapClassDirectory(SignUpController::class, __DIR__ . '/../controllers/SignUpController.php');
            $serviceProvider->mapClassDirectory(LoginController::class, __DIR__ . '/../controllers/LoginController.php');
            $serviceProvider->mapClassDirectory(IndexController::class, __DIR__ . '/../controllers/IndexController.php');
            $serviceProvider->mapClassDirectory(DeployController::class, __DIR__ . '/../controllers/DeployController.php');
            $serviceProvider->mapClassDirectory(DashboardController::class, __DIR__ . '/../controllers/DashboardController.php');
            $serviceProvider->mapClassDirectory(Logger::class, __DIR__ . '/../logs/Logger.php');
            $serviceProvider->mapClassDirectory(OldLogFileRemover::class, __DIR__ . '/../logs/OldLogFileRemover.php');
            $serviceProvider->mapClassDirectory(DeployLogger::class, __DIR__ . '/../logs/DeployLogger.php');
            $serviceProvider->mapClassDirectory(DataSourceLogger::class, __DIR__ . '/../logs/DataSourceLogger.php');
            $serviceProvider->mapClassDirectory(AccessLogger::class, __DIR__ . '/../logs/AccessLogger.php');
            $serviceProvider->mapClassDirectory(CustomSQLite3::class, __DIR__ . '/../db/CustomSQLite3.php');
            $serviceProvider->mapClassDirectory(ArrayUtils::class, __DIR__ . '/../services/service_models/ArrayUtils.php');
            $serviceProvider->mapClassDirectory(Session::class, __DIR__ . '/../services/service_models/Session.php');
            $serviceProvider->mapClassDirectory(Authenticator::class, __DIR__ . '/../services/service_models/Authenticator.php');
            $serviceProvider->mapClassDirectory(Request::class, __DIR__ . '/../services/service_models/Request.php');
            $serviceProvider->mapClassDirectory(Response::class, __DIR__ . '/../services/service_models/Response.php');
            $serviceProvider->mapClassDirectory(ResponseHandler::class, __DIR__ . '/../services/service_models/ResponseHandler.php');
            $serviceProvider->mapClassDirectory(StringUtils::class, __DIR__ . '/../services/service_models/StringUtils.php');
            $serviceProvider->mapClassDirectory(Router::class, __DIR__ . '/../routes/Router.php');
            $serviceProvider->mapClassDirectory(Sitemap::class, __DIR__ . '/../services/service_models/Sitemap.php');
            $serviceProvider->mapClassDirectory(Robots::class, __DIR__ . '/../services/service_models/Robots.php');
            $serviceProvider->mapClassDirectory(CustomEnv::class, __DIR__ . '/../services/service_models/CustomEnv.php');
            $serviceProvider->mapClassDirectory(View::class, __DIR__ . '/../views/View.php');
            
            /**
             * service register setting
             */
            // Utils
            $serviceProvider->add(IEnv::class, CustomEnv::class, ServiceLifetimeType::SINGLETON);
            $serviceProvider->add(IArrayUtils::class, ArrayUtils::class);
            $serviceProvider->add(IStringUtils::class, StringUtils::class);
            $serviceProvider->add(ISession::class, Session::class);
            $serviceProvider->add(IAuthenticator::class, Authenticator::class);
            $serviceProvider->add(IRequest::class, Request::class);
            $serviceProvider->add(IResponse::class, Response::class);
            $serviceProvider->add(IResponseHandler::class, ResponseHandler::class);
            $serviceProvider->add(IRouter::class, Router::class, ServiceLifetimeType::SINGLETON);
            $serviceProvider->add(ISitemap::class, Sitemap::class);
            $serviceProvider->add(IRobots::class, Robots::class);
            $serviceProvider->add(IView::class, View::class);
            // Loggers
            $serviceProvider->add(IOldLogFileRemover::class, OldLogFileRemover::class);
            $serviceProvider->add(ILogger::class, Logger::class);
            $serviceProvider->add(IDeployLogger::class, DeployLogger::class);
            $serviceProvider->add(IDataSourceLogger::class, DataSourceLogger::class);
            $serviceProvider->add(IAccessLogger::class, AccessLogger::class);
            // Parsers
            $serviceProvider->add(IUserModelBuilder::class, UserModelBuilder::class);
            // Repositories
            $serviceProvider->add(CustomSQLite3::class, CustomSQLite3::class, ServiceLifetimeType::SINGLETON);
            $serviceProvider->add(IUserRepository::class, UserRepository::class);
            $serviceProvider->add(IArrayUserRepository::class, ArrayUserRepository::class);
            // Controllers
            $serviceProvider->add(ISignUpController::class, SignUpController::class);
            $serviceProvider->add(ILoginController::class, LoginController::class);
            $serviceProvider->add(IIndexController::class, IndexController::class);
            $serviceProvider->add(IDeployController::class, DeployController::class);
            $serviceProvider->add(IDashboardController::class, DashboardController::class);

            self::$serviceProvider = $serviceProvider;
        }

        return $serviceProvider;
    }
}