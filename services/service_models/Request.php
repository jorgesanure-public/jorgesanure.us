<?php

require_once __DIR__ . '/../service_models/interfaces/IRequest.php';
require_once __DIR__ . '/../service_models/interfaces/IArrayUtils.php';
require_once __DIR__ . '/../service_models/interfaces/IStringUtils.php';
require_once __DIR__ . '/../../routes/RequestMethod.php';

class Request implements IRequest
{
    private IArrayUtils $arrayUtils;
    private IStringUtils $stringUtils;

    public function __construct(IArrayUtils $arrayUtils, IStringUtils $stringUtils)
    {
        $this->arrayUtils = $arrayUtils;
        $this->stringUtils = $stringUtils;
    }

    public function isPost(): bool
    {
        return $this->getFromServer('REQUEST_METHOD') === RequestMethod::POST;
    }
    
    public function isGet(): bool
    {
        return $this->getFromServer('REQUEST_METHOD') === RequestMethod::GET;
    }

    public function getMethod(): ?string
    {
        return $this->stringUtils->buildString($this->getFromServer('REQUEST_METHOD'));
    }

    public function getUri(): ?string
    {
        return $this->stringUtils->buildString($this->getFromServer('REQUEST_URI'));
    }

    public function getRequestSchema(): ?string
    {
        return $this->stringUtils->buildString($this->getFromServer('REQUEST_SCHEME'));
    }

    public function getHttpHost(): ?string
    {
        return $this->stringUtils->buildString($this->getFromServer('HTTP_HOST'));
    }

    public function getHostWithSchema(): string
    {
        return $this->getRequestSchema() . '://' . $this->getHttpHost();
    }

    public function getFromServer(?string $key): mixed
    {
        return $key === null ? null : $this->arrayUtils->getValueByKeyFromArray($key, $_SERVER);
    }

    public function getFromPostParams(?string $key): mixed
    {
        return $key === null ? null : $this->arrayUtils->getValueByKeyFromArray($key, $_POST);
    }

    public function getFromGetParams(?string $key): mixed
    {
        return $this->arrayUtils->getValueByKeyFromArray($key, $_GET);
    }

    public function redirectTo(string $routeName, ?array $data = null): void
    {
        $params = [];
        if (! empty($data)) {
            foreach ($data as $key => $value) {
                /** @phpstan-ignore-next-line */
                $params[] = "$key=$value";
            }
        }
        $params = implode('&', $params);
        
        header("location: $routeName" . (empty($params) ? '' : "?$params"));
        exit;
    }

    public function isApi(): bool
    {
        $uri = $this->getUri();
        return is_string($uri) && strpos($uri, '/api/') !== false;
    }

    public function getUriWithoutParams(): string
    {
        $uri = $this->getUri();
        $uri = $uri === null || empty($uri) ? '' : strtok($uri, '?');
        return $uri;
    }
}