<?php

require_once __DIR__ . '/interfaces/IRequest.php';
require_once __DIR__ . '/interfaces/IRobots.php';

class Robots implements IRobots
{
    private IRequest $request;

    public function __construct(IRequest $request)
    {
        $this->request = $request;
    }

    public function createRobotsFile(): void
    {
        $filename = __DIR__ . '/../../public/robots.txt';
        $host = $this->request->getHostWithSchema();

        $content = "User-agent: *
Crawl-delay: 10
Sitemap: $host/sitemap.xml";

        $fh = fopen($filename, 'w');

        if ($fh !== false) {
            fwrite($fh, $content);
            fclose($fh);
        }
    }
}