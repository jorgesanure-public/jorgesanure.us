<?php

require_once __DIR__ . '/../../routes/interfaces/IRouter.php';
require_once __DIR__ . '/interfaces/IRequest.php';
require_once __DIR__ . '/interfaces/ISitemap.php';

class Sitemap implements ISitemap
{
    private IStringUtils $stringUtils;
    private IRequest $request;
    private IRouter $router;

    public function __construct(IStringUtils $stringUtils, IRequest $request, IRouter $router)
    {
        $this->stringUtils = $stringUtils;
        $this->request = $request;
        $this->router = $router;
    }

    public function buildContent(): ?string
    {
        $host = $this->request->getHostWithSchema();
        $urlPathList = $this->router->getAllSitemapRouteNames();

        $urlListXml = '';
        foreach ($urlPathList as $path) {
            $urlListXml .= trim($this->buildXmlUrlTag($host, $path));
        }

        $sitemapXml = "<urlset xmlns='http://www.sitemaps.org/schemas/sitemap/0.9'>$urlListXml</urlset>";

        $sitemapXml = $this->stringUtils->removeBreaklines(trim($sitemapXml));

        return $sitemapXml;
    }

    private function buildXmlUrlTag(string $host, string $path): string
    {
        return "<url><loc>$host$path</loc></url>";
    }

    public function createSitemapFile(): void
    {
        $filename = __DIR__ . '/../../public/sitemap.xml';
        $content = $this->buildContent() ?? '';
        $fh = fopen($filename, 'w');
        
        if ($fh !== false) {
            fwrite($fh, $content);
            fclose($fh);
        }
    }
}