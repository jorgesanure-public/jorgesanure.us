<?php

interface IArrayUtils
{
    /** @param array<mixed> $array */
    public function getValueByKeyFromArray(?string $key, ?array $array, bool $trimFlag = true): mixed;
}