<?php

interface ISitemap
{
    public function createSitemapFile(): void;
}