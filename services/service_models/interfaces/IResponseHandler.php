<?php

require_once __DIR__ . '/IResponse.php';

interface IResponseHandler
{
    public function resolve(?callable $defaultCallback = null): void;
    public function reject(?callable $defaultCallback = null): void;
    public function setData(mixed $data): IResponseHandler;
    public function getData(): mixed;
    public function getResponse(): IResponse;
}