<?php

interface IAuthenticator
{
    public function authenticate(?string $username): void;
    public function isAuthenticated(): bool;
    public function getUsername(): string;
    public function logout(): void;
}