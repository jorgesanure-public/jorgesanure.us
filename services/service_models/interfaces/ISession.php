<?php

interface ISession
{
    public function get(?string $key): mixed;
    public function getOnce(?string $key): mixed;
    public function set(?string $key, mixed $value): void;
    public function clear(): void;
}