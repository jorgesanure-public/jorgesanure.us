<?php

interface IResponse
{
    public function buildResponseMessage(mixed $message): void;
    public function responseBadRequest(mixed $message = 'Bad Request'): void;
    public function responseNotFound(mixed $message = 'Not Found'): void;
    public function responseUnauthorized(mixed $message = 'Unauthorized'): void;
    public function responseUnexpectedErrorForUser(): void;
    public function responseUnexpectedError(mixed $message = ''): void;
    public function responseOK(mixed $message = ''): void;
}