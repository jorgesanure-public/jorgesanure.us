<?php

require_once __DIR__ . '/interfaces/IResponseHandler.php';
require_once __DIR__ . '/interfaces/IRequest.php';
require_once __DIR__ . '/interfaces/IResponse.php';

class ResponseHandler implements IResponseHandler
{
    private IRequest $request;
    private IResponse $response;
    private mixed $data;

    public function __construct(IRequest $request, IResponse $response)
    {
        $this->request = $request;
        $this->response = $response;
    }

    public function setData(mixed $data): IResponseHandler
    {
        $this->data = $data;
        return $this;
    }

    public function getData(): mixed
    {
        return $this->data;
    }

    public function resolve(?callable $defaultCallback = null): void
    {
        if ($this->request->isApi()) {
            $this->response->responseOK($this->getData());
        } else if (is_callable($defaultCallback)) {
            $defaultCallback();
        }
    }

    public function reject(?callable $defaultCallback = null): void
    {
        if ($this->request->isApi()) {
            $this->response->responseUnexpectedError($this->getData());
        } else if (is_callable($defaultCallback)) {
            $defaultCallback();
        }
    }

    public function getResponse(): IResponse
    {
        return $this->response;
    }
}