<?php

require_once __DIR__ . '/Middleware.php';
require_once __DIR__ . '/../services/service_models/interfaces/IRequest.php';
require_once __DIR__ . '/RequestMethod.php';
require_once __DIR__ . '/interfaces/IRouter.php';
require_once __DIR__ . '/interfaces/IRoute.php';
require_once __DIR__ . '/Route.php';

class Router implements IRouter
{
    /** @var array<string, IRoute> $routes */
    private array $routes;
    private IRequest $request;

    public function __construct(IRequest $request)
    {
        if (empty($this->routes)) {
            $this->routes = [];
        }

        $this->request = $request;
    }

    public function addRoute(IRoute $route): void
    {
        $isCallable = is_callable($route->getCallback());

        if (
            ! $isCallable
            || empty($route->getRouteName())
            || empty($route->getMethod())
        ) {
            $msg = "Error adding route to router: route name '" . $route->getRouteName() . "' method '" . $route->getMethod()?->name . "' is callable '" . $isCallable . "'";
            throw new Exception($msg, 1);
        }

        $this->routes[$this->buildRouteKey($route->getRouteName(), $route->getMethod()->name)] = $route;
    }

    public function getAllSitemapRoutes(): array
    {
        $publicRoutes = [];
        foreach (($this->routes ?? []) as $route) {
            if ($route->showOnSitemap()) {
                $publicRoutes[] = $route;
            }
        }
        return $publicRoutes;
    }

    public function getAllSitemapRouteNames(): array
    {
        return array_map(function($route) { return $route->getRouteName() ?? ''; }, $this->getAllSitemapRoutes());
    }

    private function buildRouteKey(string $name, ?string $method): string
    {
        if ($method === null) {
            throw new Exception("Error Processing Request: name '$name' method '$method'", 1);
        }

        return $method . '_' . $name;
    }

    public function getRouteByName(string $name, ?string $method = null): ?IRoute
    {
        $route = null;
        $routeKey = $this->buildRouteKey($name, $method ?? $this->request->getMethod());

        if (array_key_exists($routeKey, $this->routes)) {
            $route = $this->routes[$routeKey];
        }

        return $route;
    }
}