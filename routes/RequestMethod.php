<?php

enum RequestMethod: string
{
    case GET = 'GET';
    case POST = 'POST';
}