<?php

/**
 * Middleware is call before routing.
 * Routes invoke middleware check method.
 */

class Middleware
{
    private string $name;
    private mixed $callback;

    /** @var array<Middleware> $middlewareList */
    private static ?array $middlewareList;

    public function __construct(string $name, mixed $callback)
    {
        $this->name = $name;
        $this->callback = $callback;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function check(): bool
    {
        $passed = false;

        if (is_callable($this->callback)) {
            $passed = ($this->callback)();
        }

        return $passed;
    }

    public static function get(string $name): ?Middleware
    {
        $middleware = null;
        
        if (empty(self::$middlewareList)) {
            self::$middlewareList = [];
        }

        if (array_key_exists($name, self::$middlewareList)) {
            $middleware = self::$middlewareList[$name];
        }

        return $middleware;
    }

    public static function set(string $name, mixed $callback): void
    {
        $middleware = new Middleware($name, $callback);

        if (empty(self::$middlewareList)) {
            self::$middlewareList = [];
        }
        self::$middlewareList[$name] = $middleware;
    }
}