<?php

require_once __DIR__ . '/IRoute.php';

interface IRouter
{
    public function addRoute(IRoute $route): void;
    /** @return array<IRoute> */
    public function getAllSitemapRoutes(): array;
    /** @return array<string> */
    public function getAllSitemapRouteNames(): array;
    public function getRouteByName(string $name, ?string $method = null): ?IRoute;
}