<?php

require_once __DIR__ . '/interfaces/IRouter.php';
require_once __DIR__ . '/RouterEnum.php';
require_once __DIR__ . '/Middleware.php';
require_once __DIR__ . '/Route.php';
require_once __DIR__ . '/interfaces/IRoute.php';
require_once __DIR__ . '/../controllers/interfaces/ILoginController.php';
require_once __DIR__ . '/../controllers/interfaces/ISignUpController.php';
require_once __DIR__ . '/../controllers/interfaces/IIndexController.php';
require_once __DIR__ . '/../controllers/interfaces/IDeployController.php';
require_once __DIR__ . '/../controllers/interfaces/IDashboardController.php';
require_once __DIR__ . '/../services/service_models/interfaces/IRequest.php';
require_once __DIR__ . '/../services/service_models/interfaces/IResponse.php';
require_once __DIR__ . '/../services/service_models/interfaces/IAuthenticator.php';
require_once __DIR__ . '/../exceptions/LogoutNotWorkingException.php';
require_once __DIR__ . '/../exceptions/ServiceProviderNotFoundException.php';

if (! isset($serviceProvider)) throw new ServiceProviderNotFoundException();

/** @var IRouter $router */
$router = $serviceProvider->resolve(IRouter::class);

Middleware::set('auth', function () use ($serviceProvider) {
    $isAuthenticated = $serviceProvider->resolve(IAuthenticator::class)->isAuthenticated();
    $request = $serviceProvider->resolve(IRequest::class);
    $response = $serviceProvider->resolve(IResponse::class);

    if (! $isAuthenticated) {
        if ($request->isApi()) {
            $response->responseUnauthorized();
        } else {
            $request->redirectTo('/login');
        }
    }

    return $isAuthenticated;
});

Middleware::set('guest', function () use ($serviceProvider) {
    $isAuthenticated = $serviceProvider->resolve(IAuthenticator::class)->isAuthenticated();
    $request = $serviceProvider->resolve(IRequest::class);
    $response = $serviceProvider->resolve(IResponse::class);

    if ($isAuthenticated) {
        if ($request->isApi()) {
            $response->responseUnauthorized('You must to logout first.');
        } else {
            $request->redirectTo('/dashboard');
        }
    }

    return ! $isAuthenticated;
});

$router->addRoute(
    Route::create()
        ->setRouteName('/')
        ->setMethod(RequestMethod::GET)
        ->setCallback(function() use ($serviceProvider) {
            $serviceProvider->resolve(IIndexController::class)->index();
        })
);

$router->addRoute(
    Route::create()
        ->setRouteName('/login')
        ->setMethod(RequestMethod::GET)
        ->setCallback(function() use ($serviceProvider) {
            $serviceProvider->resolve(ILoginController::class)->showLoginForm();
        })
        ->setMiddlewares(['guest'])
);

$router->addRoute(
    Route::create()
        ->setRouteName('/login')
        ->setMethod(RequestMethod::POST)
        ->setCallback(function() use ($serviceProvider) {
            $serviceProvider->resolve(ILoginController::class)->login();
        })
        ->setMiddlewares(['guest'])
);

$router->addRoute(
    Route::create()
        ->setRouteName('/api/login')
        ->setMethod(RequestMethod::POST)
        ->setCallback(function() use ($serviceProvider) {
            $serviceProvider->resolve(ILoginController::class)->login();
        })
        ->setMiddlewares(['guest'])
);

$router->addRoute(
    Route::create()
        ->setRouteName('/logout')
        ->setMethod(RequestMethod::GET)
        ->setCallback(function() use ($serviceProvider) {
            $serviceProvider->resolve(ILoginController::class)->logout();
        })
        ->setMiddlewares(['auth'])
);

$router->addRoute(
    Route::create()
        ->setRouteName('/api/logout')
        ->setMethod(RequestMethod::GET)
        ->setCallback(function() use ($serviceProvider) {
            $serviceProvider->resolve(ILoginController::class)->logout();
        })
        ->setMiddlewares(['auth'])
);

$router->addRoute(
    Route::create()
        ->setRouteName('/signup')
        ->setMethod(RequestMethod::GET)
        ->setCallback(function() use ($serviceProvider) {
            $serviceProvider->resolve(ISignUpController::class)->showSignUpForm();
        })
        ->setMiddlewares(['guest'])
);

$router->addRoute(
    Route::create()
        ->setRouteName('/signup')
        ->setMethod(RequestMethod::POST)
        ->setCallback(function() use ($serviceProvider) {
            $serviceProvider->resolve(ISignUpController::class)->signup();
        })
        ->setMiddlewares(['guest'])
);

$router->addRoute(
    Route::create()
        ->setRouteName('/api/signup')
        ->setMethod(RequestMethod::POST)
        ->setCallback(function() use ($serviceProvider) {
            $serviceProvider->resolve(ISignUpController::class)->signup();
        })
        ->setMiddlewares(['guest'])
);

$router->addRoute(
    Route::create()
        ->setRouteName('/deploy')
        ->setMethod(RequestMethod::POST)
        ->setCallback(function() use ($serviceProvider) {
            $serviceProvider->resolve(IDeployController::class)->deploy();
        })
        ->setShowOnSitemap(false)
        ->setRequireSession(false)
);

$router->addRoute(
    Route::create()
        ->setRouteName('/dashboard')
        ->setMethod(RequestMethod::GET)
        ->setCallback(function() use ($serviceProvider) {
            $serviceProvider->resolve(IDashboardController::class)->index();
        })
        ->setMiddlewares(['auth'])
);