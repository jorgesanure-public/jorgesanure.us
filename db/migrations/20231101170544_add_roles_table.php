<?php

declare(strict_types=1);

use Phinx\Migration\AbstractMigration;

final class AddRolesTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change(): void
    {
        $this->table('roles')
            ->addColumn('name', 'string', ['null' => false])
            ->addColumn('code', 'string', ['null' => false])
            ->addColumn('is_active', 'boolean', ['null' => false, 'default' => 1])
            ->addTimestamps()
            ->addIndex('name', ['unique' => true])
            ->addIndex('code', ['unique' => true])
            ->create();
    }
}
