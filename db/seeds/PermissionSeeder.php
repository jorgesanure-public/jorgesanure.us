<?php


use Phinx\Seed\AbstractSeed;

class PermissionSeeder extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * https://book.cakephp.org/phinx/0/en/seeding.html
     */
    public function run(): void
    {
        $table = $this->table('permissions');

        $table->truncate();

        $data = [
            [
                'name' => 'List Users',
                'code' => 'LU',
            ],
            [
                'name' => 'See Other User Detail',
                'code' => 'SOUD',
            ],
            [
                'name' => 'Delete Other User',
                'code' => 'DOU',
            ],
            [
                'name' => 'Edit Other User',
                'code' => 'EOU',
            ],
        ];

        $table->insert($data)
            ->saveData();
    }
}
