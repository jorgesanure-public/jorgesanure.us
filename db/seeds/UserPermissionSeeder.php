<?php


use Phinx\Seed\AbstractSeed;

class UserPermissionSeeder extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * https://book.cakephp.org/phinx/0/en/seeding.html
     */
    public function run(): void
    {
        $table = $this->table('user_permissions');

        $table->truncate();

        $permissionCodeList = ['SOUD', 'DOU', 'EOU', 'LU'];
        $userIdList = ['1'];

        $data = [];
        foreach ($userIdList as $userId) {
            foreach ($permissionCodeList as $permissionCode) {
                $data[] = [
                    'user_id' => $userId,
                    'permission_code' => $permissionCode
                ];
            }
        }

        $table->insert($data)
            ->saveData();
    }
}
