<?php


use Phinx\Seed\AbstractSeed;

class UserRoleSeeder extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * https://book.cakephp.org/phinx/0/en/seeding.html
     */
    public function run(): void
    {
        $table = $this->table('user_roles');

        $table->truncate();

        $data = [
            [
                'user_id' => '1',
                'role_code' => 'SA',
            ],
            [
                'user_id' => '1',
                'role_code' => 'A',
            ],
        ];

        $table->insert($data)
            ->saveData();
    }
}
