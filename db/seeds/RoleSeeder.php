<?php


use Phinx\Seed\AbstractSeed;

class RoleSeeder extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * https://book.cakephp.org/phinx/0/en/seeding.html
     */
    public function run(): void
    {
        $table = $this->table('roles');

        $table->truncate();

        $data = [
            [
                'name' => 'System Admin',
                'code' => 'SA',
            ],
            [
                'name' => 'Admin',
                'code' => 'A',
            ],
        ];

        $table->insert($data)
            ->saveData();
    }
}
