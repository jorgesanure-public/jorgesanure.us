<?php

try {
    require_once __DIR__ . '/../logs/interfaces/ILogger.php';
    require_once __DIR__ . '/../logs/OldLogFileRemover.php';
    require_once __DIR__ . '/../services/service_models/StringUtils.php';
    require_once __DIR__ . '/../services/service_models/CustomEnv.php';

    try {
        $serviceProvider = null;

        require_once __DIR__ . '/../services/ServiceProviderBuilder.php';
        require_once __DIR__ . '/../services/service_models/interfaces/IRequest.php';
        require_once __DIR__ . '/../services/service_models/interfaces/IResponse.php';
        require_once __DIR__ . '/../services/service_models/interfaces/IEnv.php';
        require_once __DIR__ . '/../logs/interfaces/IAccessLogger.php';
        require_once __DIR__ . '/../exceptions/ServiceProviderNotFoundException.php';
        require_once __DIR__ . '/../routes/interfaces/IRouter.php';

        $serviceProviderBuilder = new ServiceProviderBuilder();
        $serviceProvider = $serviceProviderBuilder->build();

        if ($serviceProvider === null) throw new ServiceProviderNotFoundException();

        /** @var IRequest $request */
        $request = $serviceProvider->resolve(IRequest::class);
        /** @var IResponse $response */
        $response = $serviceProvider->resolve(IResponse::class);
        /** @var IAccessLogger $accessLogger */
        $accessLogger = $serviceProvider->resolve(IAccessLogger::class);
        /** @var ILogger $logger */
        $logger = $serviceProvider->resolve(ILogger::class);
        /** @var IRouter $router */
        $router = $serviceProvider->resolve(IRouter::class);
        /** @var IEnv $env */
        $env = $serviceProvider->resolve(IEnv::class);

        // Set the custom error handler for warnings
        set_error_handler(
            function (int $errno, string $errstr, string $errfile, int $errline) use ($logger): bool
            {
                // Handle the warning as you see fit
                $logger->logWarning($errstr, $errfile, $errline);

                // if false, it will print warning message on screen as well
                return true;
            },
            E_WARNING
        );

        $uri = $request->getUri();

        try {
            require_once __DIR__ . '/../routes/routes.php';
        
            $env->init();

            $accessLogger->log($request->getMethod() . ' ' . $uri);
            
            // take out url params to get only url path 
            $uriWithoutParams = strtok($uri ?? '', '?');

            if ($uriWithoutParams !== false) {
                $route = $router->getRouteByName($uriWithoutParams);
                
                if (empty($route)) {
                    $response->responseNotFound();
                } else {
                    $route->route();
                }
            } else {
                $response->responseNotFound();
            }
        } catch (\Throwable $e) {
            $logger->logError("[uri: $uri] " . $e->getMessage(),  $e->getFile(), $e->getLine());
            $response->responseUnexpectedError();
        }

    } catch (\Throwable $e) {
        (new Logger(new OldLogFileRemover(), new StringUtils(), new CustomEnv()))->logError($e->getMessage() . ' ' . $e->getFile() . ':' . $e->getLine());
        http_response_code(500);
    }
} catch (\Throwable $th) {
    $msg = $th->getMessage() . ' ' . $th->getFile() . ':' . $th->getLine();
    error_log($msg);
    http_response_code(500);
}

exit;