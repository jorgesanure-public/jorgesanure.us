<?php

require_once __DIR__ . '/../../services/ServiceProviderBuilder.php';
require_once __DIR__ . '/../../services/service_models/interfaces/IEnv.php';

$serviceProviderBuilder = new ServiceProviderBuilder();
$serviceProvider = $serviceProviderBuilder->build();

if ($serviceProvider === null) throw new Exception("Error Processing Request", 1);

/** @var IEnv $env */
$env = $serviceProvider->resolve(IEnv::class);
$env->init();

return
[
    'paths' => [
        'migrations' => __DIR__ . '/../../db/migrations',
        'seeds' => __DIR__ . '/../../db/seeds'
    ],
    'environments' => [
        'default_migration_table' => 'phinxlog',
        'default_database' => $env->get('PHINX_DEFAULT_DATABASE', 'production'),
        'production' => [
            'adapter' => 'sqlite',
            'name' => __DIR__ . '/../../db/production',
        ],
        'development' => [
            'adapter' => 'sqlite',
            'name' => __DIR__ . '/../../db/development',
        ],
        'testing' => [
            'adapter' => 'sqlite',
            'name' => __DIR__ . '/../../db/testing',
        ],
    ],
    'version_order' => 'creation'
];
