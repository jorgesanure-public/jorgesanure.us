# [jorgesanure.us](http://jorgesanure.us/)

Server side php api to response to external client side projects.

## Database

Use `vendor/bin/phinx migrate` and `vendor/bin/phinx seed:run` to set up database

## Deployment

Each push made to `production` branch will trigger a `webhook` request to apply branch changes into server.

Run `source .bashrc` from root directory.
Run `php vendor/bin/phinx migrate` from project directory.
Run `php vendor/bin/phinx seed:run` from project directory.

#### Downside using webhooks ?
- Runtime errors could prevent updating server to get new changes, even when new change includes the fix.

## Lint

Check code running following script: `.\vendor\bin\phpstan analyze`