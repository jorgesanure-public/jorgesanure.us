<?php

require_once __DIR__ . '/../views/interfaces/IView.php';
require_once __DIR__ . '/interfaces/IIndexController.php';
require_once __DIR__ . '/../services/service_models/interfaces/IRequest.php';
require_once __DIR__ . '/../services/service_models/interfaces/IAuthenticator.php';

class IndexController implements IIndexController
{
    private IAuthenticator $authenticator;
    private IView $view;

    public function __construct(IAuthenticator $authenticator, IView $view)
    {
        $this->authenticator = $authenticator;
        $this->view = $view;
    }

    public function index(): void
    {
        $this->view->build('index/index.php', [
            'authenticator' => $this->authenticator
        ]);
    }
}