<?php

require_once __DIR__ . '/../views/interfaces/IView.php';
require_once __DIR__ . '/interfaces/IDashboardController.php';
require_once __DIR__ . '/../services/service_models/interfaces/IAuthenticator.php';

class DashboardController implements IDashboardController
{
    private IAuthenticator $authenticator;
    private IView $view;

    public function __construct(IAuthenticator $authenticator, IView $view)
    {
        $this->authenticator = $authenticator;
        $this->view = $view;
    }

    public function index(): void
    {
        $this->view->build('dashboard/index.php', [
            'username' => $this->authenticator->getUsername()
        ]);
    }
}