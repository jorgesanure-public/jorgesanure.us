<?php

interface IDeployController
{
    public function deploy(): void;
}