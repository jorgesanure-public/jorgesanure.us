<?php

interface IIndexController
{
    public function index(): void;
}