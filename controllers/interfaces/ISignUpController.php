<?php

interface ISignUpController
{
    public function showSignUpForm(): void;
    public function signUp(): void;
}