<?php

interface ILoginController
{
    public function showLoginForm(): void;
    public function login(): void;
    public function logout(): void;
}