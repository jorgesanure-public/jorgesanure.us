<?php

require_once __DIR__ . '/../services/service_models/interfaces/IAuthenticator.php';
require_once __DIR__ . '/../services/service_models/interfaces/IRequest.php';
require_once __DIR__ . '/../services/service_models/interfaces/IResponseHandler.php';
require_once __DIR__ . '/../logs/interfaces/ILogger.php';
require_once __DIR__ . '/../services/service_models/interfaces/ISession.php';

abstract class BaseController
{
    protected IAuthenticator $authenticator;
    protected IRequest $request;
    protected IResponseHandler $responseHandler;
    protected ILogger $logger;
    protected ISession $session;

    public function __construct(IAuthenticator $authenticator, IRequest $request, IResponseHandler $responseHandler, ILogger $logger, ISession $session)
    {
        $this->authenticator = $authenticator;
        $this->request = $request;
        $this->responseHandler = $responseHandler;
        $this->logger = $logger;
        $this->session = $session;
    }
}