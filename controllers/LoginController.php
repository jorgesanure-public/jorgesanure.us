<?php

require_once __DIR__ . '/../views/interfaces/IView.php';
require_once __DIR__ . '/interfaces/ILoginController.php';
require_once __DIR__ . '/BaseController.php';
require_once __DIR__ . '/../services/service_models/SessionKeys.php';
require_once __DIR__ . '/../repositories/interfaces/IUserRepository.php';
require_once __DIR__ . '/../services/service_models/interfaces/IRequest.php';
require_once __DIR__ . '/../services/service_models/interfaces/IResponseHandler.php';
require_once __DIR__ . '/../services/service_models/interfaces/IAuthenticator.php';

class LoginController extends BaseController implements ILoginController
{
    private IUserRepository $userRepository;
    private IView $view;

    public function __construct(IUserRepository $userRepository, IAuthenticator $authenticator, IRequest $request, IResponseHandler $responseHandler, ILogger $logger, ISession $session, IView $view)
    {
        $this->userRepository = $userRepository;
        $this->view = $view;
        parent::__construct($authenticator, $request, $responseHandler, $logger, $session);
    }

    public function showLoginForm(): void
    {
        $userErrorMsg = $this->session->getOnce(SessionKeys::USER_ERROR_MESSAGE);
        $loginUsername = $this->session->getOnce('loginUsername');

        $this->view->build('login/login_form.php', [
            'userErrorMsg' => $userErrorMsg,
            'loginUsername' => $loginUsername
        ]);
    }

    public function login(): void
    {
        $givenUsername = $this->request->getFromPostParams('username');
        $givenPassword = $this->request->getFromPostParams('password');

        if (is_string($givenUsername) === false) {
            $givenUsername = '';
        }
        if (is_string($givenPassword) === false) {
            $givenPassword = '';
        }

        $user = $this->userRepository->findByUsername($givenUsername);

        if (
            $user !== null
            && password_verify($givenPassword, $user->password ?? '')
        ) {
            $this->authenticator->authenticate($givenUsername);
        } else {
            $this->session->set(SessionKeys::USER_ERROR_MESSAGE, 'Invalid credentials');
            $this->session->set('loginUsername', $givenUsername);
        }

        if ($this->authenticator->isAuthenticated()) {
            $this->responseHandler->resolve(function() {
                $this->request->redirectTo('/dashboard');
            });
        } else {
            $this->responseHandler
                ->setData($this->session->get(SessionKeys::USER_ERROR_MESSAGE))
                ->reject(function() {
                $this->request->redirectTo('/login');
            });
        }
    }

    public function logout(): void
    {
        $this->authenticator->logout();

        $this->responseHandler->resolve(function() {
            $this->request->redirectTo('/');
        });
    }
}