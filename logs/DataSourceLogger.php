<?php

require_once __DIR__ . '/BaseLogger.php';
require_once __DIR__ . '/interfaces/ILogger.php';

class DataSourceLogger extends BaseLogger implements IDataSourceLogger
{
    protected function getCurrentLogFilename(): string
    {
        return 'data_source_log';
    }
}