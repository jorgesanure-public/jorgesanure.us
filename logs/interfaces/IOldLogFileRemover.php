<?php

interface IOldLogFileRemover
{
    public function remove(string $directory, ILogger $logger): void;
}