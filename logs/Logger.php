<?php

require_once __DIR__ . '/BaseLogger.php';
require_once __DIR__ . '/interfaces/ILogger.php';

class Logger extends BaseLogger implements ILogger {}