<?php

require_once __DIR__ . '/BaseLogger.php';
require_once __DIR__ . '/interfaces/IDeployLogger.php';

class DeployLogger extends BaseLogger implements IDeployLogger
{
    protected function getCurrentLogFilename(): string
    {
        return 'deploy_log';
    }

    protected function buildLogFilename(string $logName, bool $includeDate = true): string
    {
        return parent::buildLogFilename($logName, false);
    }
}