<?php

require_once __DIR__ . '/BaseLogger.php';
require_once __DIR__ . '/interfaces/IAccessLogger.php';

class AccessLogger extends BaseLogger implements IAccessLogger
{
    protected function getCurrentLogFilename(): string
    {
        return 'access_log';
    }
}