<?php

namespace Sqlite3RepositoryTest;

require_once __DIR__ . '/../../../models/UserModel.php';
require_once __DIR__ . '/../../../exceptions/UsernameAlreadyExistsException.php';
require_once __DIR__ . '/../../../repositories/interfaces/IUserRepository.php';
require_once __DIR__ . '/../../../services/ServiceProviderBuilder.php';
require_once __DIR__ . '/../../../services/service_models/interfaces/IEnv.php';

use IUserRepository;
use IEnv;
use PHPUnit\Framework\TestCase;
use UserModel;
use UsernameAlreadyExistsException;
use ServiceProviderBuilder;
use Env;
use Exception;

class UserRepositoryTest extends TestCase
{
    public function testThrowExeptionOnDuplicateUsernameWhenSignUpWithSqlite3Repository(): void
    {
        // Arrange
        $serviceProviderBuilder = new ServiceProviderBuilder();
        $serviceProvider = $serviceProviderBuilder->build();

        if ($serviceProvider === null) throw new Exception("Error Processing Request", 1);

        /** @var IEnv $env */
        $env = $serviceProvider->resolve(IEnv::class);
        $env->init();
        $env->set('PHINX_DEFAULT_DATABASE', 'testing');
        
        $dbFilename = __DIR__ . '\\..\\..\\..\\db\\testing.sqlite3';
        if (file_exists($dbFilename)) {
            unlink($dbFilename);
        }
        shell_exec('.\vendor\bin\phinx migrate -e testing');
        shell_exec('.\vendor\bin\phinx seed:run -e testing');

        /** @var IUserRepository $userRepository */
        $userRepository = $serviceProvider->resolve(IUserRepository::class);
        $userModel = new UserModel('username2', 'password', 'account');

        // Assert
        $this->expectException(UsernameAlreadyExistsException::class);

        // Act
        $userRepository->signUp($userModel);
        $userRepository->signUp($userModel);
    }
}