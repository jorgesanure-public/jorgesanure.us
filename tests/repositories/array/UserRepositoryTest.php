<?php

namespace ArrayRepositoryTest;

require_once __DIR__ . '/../../../models/UserModel.php';
require_once __DIR__ . '/../../../exceptions/UsernameAlreadyExistsException.php';
require_once __DIR__ . '/../../../repositories/array/interfaces/IArrayUserRepository.php';
require_once __DIR__ . '/../../../services/ServiceProviderBuilder.php';

use PHPUnit\Framework\TestCase;
use UserModel;
use UsernameAlreadyExistsException;
use ServiceProviderBuilder;
use IArrayUserRepository;
use Exception;

class UserRepositoryTest extends TestCase
{
    public function testThrowExeptionOnDuplicateUsernameWhenSignUp(): void
    {
        // Arrange
        $serviceProviderBuilder = new ServiceProviderBuilder();
        $serviceProvider = $serviceProviderBuilder->build();

        if ($serviceProvider === null) throw new Exception("Error Processing Request", 1);
        
        /** @var IArrayUserRepository $userRepository */
        $userRepository = $serviceProvider->resolve(IArrayUserRepository::class);
        $dataSource = [
            'users' => []
        ];
        $userModel = new UserModel('username', 'password', 'account');

        // Assert
        /** @phpstan-ignore-next-line */
        $this->expectException(UsernameAlreadyExistsException::class);

        // Act
        $userRepository->signUp($userModel);
        $userRepository->signUp($userModel);
    }
}