<?php
    require_once __DIR__ . '/../../services/ServiceProviderBuilder.php';
    require_once __DIR__ . '/../../services/service_models/interfaces/IRequest.php';

    $serviceProviderBuilder ??= new ServiceProviderBuilder();
    $serviceProvider ??= $serviceProviderBuilder->build();
    /** @var IRequest $request */
    $request = $serviceProvider->resolve(IRequest::class);
    $metaCanonical = $request->getHostWithSchema();
?>
<!doctype html>
<html lang="en">
    <head>
        <title>jorgesanure.us</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="Jorge Santana Personal Portfolio" />
        <meta name="keywords" content="SEO, search engine optimization, website ranking" />
        <link rel="canonical" href="<?= $metaCanonical; ?>" />
        <meta name="robots" content="index, follow" />
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous">
        <link href="/css/main.css" rel="stylesheet">
    </head>
    <body>