<?php

class ServiceProviderNotFoundException extends Exception
{
    public function __construct(string $message = "Service provider was not found.", int $code = 1, Throwable|null $previous = null) {
        parent::__construct($message, $code, $previous);
    }
}